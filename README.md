# Desarrollo de Aplicaciones WEB - Distancia
## Examen final de Programación
## 9 de junio de 2020


#### Ejercicio 1 (3 puntos)

Dado un fichero de texto en el que se almacena el log de mensajes enviados por una serie de usuarios
a través de un determinado servidor, se pretende procesar los datos en él almacenados para obtener la
siguiente información:

- Número de mensajes que envía cada usuario por cada IP desde la que se ha conectado.
- Número total de IPs desde las que ha enviado mensajes cada usuario.

El fichero de texto consta de un número indeterminado de líneas y en cada una de ellas se almacena información de un mensaje enviado por
un usuario desde una IP con el formato siguiente:

IP=*dirección_ip*;mensaje=*texto_del_mensaje*;usuario=*nombre_de_usuario*

Toda la información obtenida ha de quedar accesible a través de una única referencia a una estructura de datos dinámica. El
programa finalizará mostrando en la consola la información obtenida con el formato siguiente:


```
nombre_de_usuario (número_total_de_IPs):
dirección_ip -> número_de_mensajes
dirección_ip -> número_de_mensajes
.
.
.
nombre_de_usuario (número_total_de_IPs):
dirección_ip -> número_de_mensajes
dirección_ip -> número_de_mensajes
.
.
.
```

El problema se ha de resolver según las especificaciones siguientes:

- Se definirá una única clase llamada Ejercicio1 que solo contendrá el método main. No se permite la definición de clases adicionales.
- Se asume que el fichero de texto se ajusta al formato especificado y, por tanto, no es necesario realizar ningún tipo de comprobación para detectar errores de formato.
- Se asume que el texto de los mensajes en el fichero de texto no va a contener los caracteres ';' o '='.
- Es posible que antes y después de los caracteres ';' o '=' haya espacios en blanco que habrá que ignorar.


## Ejercicio 2 (3 puntos)

En un fichero de texto se guardan datos de compuestos químicos a razón de uno por línea con el formato siguiente:

*nombre_del_compuesto*: *elemento_químico_1* *elemento_químico_2* *elemento_químico_3* ...

Ejemplos:

```
Hidróxido de sodio: Na O H
Nitrato de potasio: K N O3
Sulfato de cobre: Cu S O4
Cloruro de sodio: Na Cl
```


Escribe un programa que lea el contenido del fichero y almacene en un TreeSet los compuestos químicos, entendiendo por compuesto
químico una colección de elementos químicos representados como cadenas y almacenados en el mismo orden en que se leen del fichero. Por
tanto, se ha de definir una clase llamada CompuestoQuímico para representar compuestos químicos según las especificaciones siguientes:

- Tiene que extender la clase que corresponda del Java Collection Framework.
- Tiene que declarar un único atributo llamado nombre para almacenar el nombre del compuesto químico, cuyo valor se puede consultar, pero no modificar.
- Tiene que definir los constructores en correspondencia con los de la superclase.
- Tiene que redefinir el método toString para que retorne el resultado de concatenar el nombre del compuesto químico con lo que retorne la versión de dicho método definida en la superclase.
- Tiene que definir un orden natural para los compuestos químicos que utilice como criterio de ordenación el orden alfabético del nombre del compuesto.

Una vez leído el contenido del fichero y almacenados los datos en la colección, muestra por pantalla todos los compuestos químicos sin
realizar ningún tipo de iteración.


## Ejercicio 3 (2 puntos)

Crear una jerarquía de clases Java para representar los electrodomésticos que se venden en una tienda Online especializada en lavadoras,
frigoríficos y televisiones. Todos los electrodomésticos comparten las siguientes características:

- Precio base con un valor por defecto de 100 €.
- Color, representado por un tipo enumerado que defina los valores BLANCO, NEGRO, ROJO, AZUL y GRIS. El valor por defecto será BLANCO.
- Consumo energético representados por una letra comprendida entre la A y la F. Su valor por defecto será la letra B.
- Peso en kg.
- Un precio final que se calcula aumentando el precio base en un porcentaje que dependerá del consumo y el peso según las tablas siguientes:

| CONSUMO | PORCENTAJE | |PESO | PORCENTAJE|
| ------ | ------ | ------ | ------ | ------ |
| A | 30% | | Entre 0 y 19 kg | 5% |
| B | 25% | | Entre 20 y 49 kg | 10% |
| C | 20% | | Entre 50 y 79 kg | 15% |
| D | 15% | | Más de 80 kg | 20% |
| E | 10% | | | |
| F | 5% | | | |

Todas las clases deben implementar o redefinir como mínimo los métodos siguientes:

- Constructores:
    - Constructor con parámetros para inicializar precio y peso.
    - Constructor con parámetros para inicializar todos los atributos.
- Getters y setters.
- Método toString.

Las lavadoras se caracterizan por soportar una carga según modelo que puede ser de 4, 5, 6, 7, 8, 10, 11 o 13 kg. El valor por defecto para la
carga será de 5 kg. Su precio final aumentará en un 10% si la capacidad de carga es mayor de 8 kg.

Los frigoríficos pueden tener o no tener capacidad No Frost. Por defecto no la tendrán.

Las televisiones se caracterizan por su tamaño en pulgadas y por el tipo de sintonizador (DVB-T o DVB-T2). Los valores por defecto serán 20
pulgadas y DVB-T respectivamente.

Se ha de definir un orden natural para todos los electrodomésticos que utilice como criterio de ordenación el tipo de electrodoméstico (orden
alfabético) y dentro de cada tipo el precio de menor a mayor.

## Ejercicio 4 (2 puntos)

Define una clase que ponga a prueba en el método main las clases definidas en el ejercicio anterior realizando las acciones siguientes:

- Almacenar en un mismo `ArrayList` electrodomésticos de todos los tipos.
- Ordenar el `ArrayList` con el método `sort(List<T> list)` de la clase `Collections`.
- Almacenar en un fichero de texto el contenido del `ArrayList` ordenado a razón de un electrodoméstico por línea. Por cada electrodoméstico se almacenará en la línea correspondiente lo que retorne el método `toString`.
- Ordenar el `ArrayList` con el método `sort(List<T> list, Comparator<? super T> c)` de la clase `Collections`, utilizando como criterio de ordenación el consumo energético de mayor consumo a menor consumo y dentro de los que tienen igual consumo, el precio de mayor a menor.
- Almacenar en un fichero binario el `ArrayList` resultado de la ordenación del punto anterior. El almacenamiento en el fichero se realizará serializando de forma independiente cada uno de los electrodomésticos almacenados en el `ArrayList`.

