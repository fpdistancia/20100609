package examen.ejercicio2;

import java.util.ArrayList;
import java.util.Collection;

public class CompuestoQuimico extends ArrayList<String> implements Comparable<CompuestoQuimico> {

	private static final long serialVersionUID = 1L;
	private String nombre;
	
	public CompuestoQuimico(String nombre) {
		super();
		this.nombre = nombre;
	}

	public CompuestoQuimico(String nombre, int initialCapacity) {
		super(initialCapacity);
		this.nombre = nombre;
	}

	public CompuestoQuimico(String nombre, Collection<? extends String> c) {
		super(c);
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	@Override
	public String toString() {
		return nombre + super.toString();
	}

	@Override
	public int compareTo(CompuestoQuimico cq) {
		return nombre.compareTo(cq.nombre);
	}

	
	
}
