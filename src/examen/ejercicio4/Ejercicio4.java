package examen.ejercicio4;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import examen.ejercicio3.Color;
import examen.ejercicio3.Electrodomestico;
import examen.ejercicio3.Frigorifico;
import examen.ejercicio3.Lavadora;
import examen.ejercicio3.Televisor;

public class Ejercicio4 {

	public static void main(String[] args) {
		List<Electrodomestico> electrodomesticos = new ArrayList<>();
		electrodomesticos.add(new Televisor(435, Color.NEGRO, 'D', 5, 43, Televisor.DVBT2));
		electrodomesticos.add(new Lavadora(650, 42));
		electrodomesticos.add(new Frigorifico(700, 57));
		electrodomesticos.add(new Lavadora(350, 37));
		Collections.sort(electrodomesticos);
		guardarFicheroTexto(electrodomesticos);
		Collections.sort(electrodomesticos, Collections.reverseOrder(new ElectrodomesticosComparator()));
		guardarFicheroBinario(electrodomesticos);
	}

	static void guardarFicheroTexto(List<Electrodomestico> electrodomesticos) {
		try (PrintWriter out = new PrintWriter(
				new OutputStreamWriter(new FileOutputStream("res/electrodomésticos.txt"), "utf8"))) {
			electrodomesticos.forEach(e -> out.println(e));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	static void guardarFicheroBinario(List<Electrodomestico> electrodomesticos) {
		try (ObjectOutputStream out = new ObjectOutputStream(
				new BufferedOutputStream(new FileOutputStream("res/electrodomésticos.bin")))) {
			for (Electrodomestico e: electrodomesticos)
				out.writeObject(e);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
