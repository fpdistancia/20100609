package examen.ejercicio3;

import java.io.Serializable;

public abstract class ElectrodomesticoV2 implements Comparable<ElectrodomesticoV2>, Serializable {
	
	private static final long serialVersionUID = 1L;
	private float precio;
	private Color color;
	private Consumo consumo;
	private float peso;
	
	public ElectrodomesticoV2(float precio, float peso) {
		this.precio = precio;
		color = Color.BLANCO;
		consumo = Consumo.B;
		this.peso = peso;
	}

	public ElectrodomesticoV2(float precio, Color color, Consumo consumo, float peso) {
		this.precio = precio;
		this.color = color;
		this.consumo = consumo;
		this.peso = peso;
	}
	
	public float getPrecioFinal() {
		float precio = this.precio;
		precio += this.precio * (1.30 - (consumo.ordinal() * 0.5));
		if (peso <= 19)
			precio += this.precio * 1.05;
		else if (peso < 49)
			precio += this.precio * 1.1;
		else if (peso < 49)
			precio += this.precio * 1.15;
		else
			precio += this.precio * 1.2;
		return precio;
	}

	public float getPrecio() {
		return precio;
	}

	public Color getColor() {
		return color;
	}

	public Consumo getConsumo() {
		return consumo;
	}

	public float getPeso() {
		return peso;
	}

	@Override
	public String toString() {
		return "[precio=" + precio + ", color=" + color + ", consumo=" + consumo + ", peso=" + peso	+ "]";
	}
	
	@Override
	public int compareTo(ElectrodomesticoV2 e) {
		int resultado = getClass().getName().compareTo(e.getClass().getName());
		if (resultado == 0)
			resultado = precio < e.getPrecio() ? -1 : precio > e.getPrecio() ? 1 : 0;
		return resultado;
	}
	
}
